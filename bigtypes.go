package api

import (
	"database/sql/driver"
	"fmt"
	"math/big"
)

// BigInt wraps big.Int to facilitate reading/writing to the database
type BigInt struct {
	*big.Int
}

// Value facilitates the writing of BigInt instances to the database
func (bi BigInt) Value() (driver.Value, error) {
	return bi.String(), nil
}

// Scan facilitates the reading of BigInt instances from the database
func (bi *BigInt) Scan(src interface{}) error {
	s := src.(string)
	b := big.NewInt(1)
	_, ok := b.SetString(s, 10)
	if !ok {
		err := fmt.Errorf("invalid big.Int value: %v", s)
		return err
	}
	bi.Int = b
	return nil
}

// BigRat wraps big.Rat to facilitate reading/writing to the database
type BigRat struct {
	*big.Rat
}

// Value facilitates the writing of BigRat instances to the database
func (br BigRat) Value() (driver.Value, error) {
	return br.String(), nil
}

// Scan facilitates the reading of BigRat instances from the database
func (br *BigRat) Scan(src interface{}) error {
	s := src.(string)
	b := big.NewRat(1, 1)
	_, ok := b.SetString(s)
	if !ok {
		err := fmt.Errorf("invalid big.Rat value: %v", s)
		return err
	}
	br.Rat = b
	return nil
}
