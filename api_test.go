package api

import (
	"math/big"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestParseGetOrdersInvertedBidsOnly(t *testing.T) {
	body := `
		{
		  "success": true,
		  "data": {
			 "buyOrders": [
				{
				  "price": "109090909090909091/20000000"
				},
				{
				  "price": "2200000000000000000/300000001"
				},
				{
				  "price": "3200000000000000000/40000002"
				}
			 ],
			 "leadCurrencyExponent": 0,
			 "market": "ETH/BTC",
			 "oppositeCurrencyExponent": 0,
			 "orderType": "1",
			 "sellOrders": [],
			 "timeRangeEnd": "2020-01-09T22:36:03+01:00",
			 "timeRangeStart": "2020-01-09T22:36:03+01:00"
		  },
		  "version": "1"
		}
	`
	pr := Pair{Base: Btc, Quote: Eth}

	ts, err := time.Parse(time.RFC3339, "2020-01-09T22:36:03+01:00")

	// buy orders need to become asks/sell orders because we flipped the
	// currency pair
	eas := []Order{
		{
			Pair:      pr,
			OrderType: Ask,
			BaseQ:     &BigInt{big.NewInt(20000000)},
			QuoteQ:    &BigInt{big.NewInt(1)},
			BigPrice:  &BigRat{big.NewRat(20000000, 1)},
			Time:      ts,
		},
		{
			Pair:      pr,
			OrderType: Ask,
			BaseQ:     &BigInt{big.NewInt(300000001)},
			QuoteQ:    &BigInt{big.NewInt(2)},
			BigPrice:  &BigRat{big.NewRat(300000001, 2)},
			Time:      ts,
		},
		{
			Pair:      pr,
			OrderType: Ask,
			BaseQ:     &BigInt{big.NewInt(40000002)},
			QuoteQ:    &BigInt{big.NewInt(3)},
			BigPrice:  &BigRat{big.NewRat(40000002, 3)},
			Time:      ts,
		},
	}
	_, ok := eas[0].QuoteQ.SetString("109090909090909091", 10)
	assert.True(t, ok)
	_, ok = eas[0].BigPrice.SetString("20000000/109090909090909091")
	assert.True(t, ok)
	_, ok = eas[1].QuoteQ.SetString("2200000000000000000", 10)
	assert.True(t, ok)
	_, ok = eas[1].BigPrice.SetString("300000001/2200000000000000000")
	assert.True(t, ok)
	_, ok = eas[2].QuoteQ.SetString("3200000000000000000", 10)
	assert.True(t, ok)
	_, ok = eas[2].BigPrice.SetString("40000002/3200000000000000000")
	assert.True(t, ok)

	var ebs []Order

	bs, as, err := parseGetOrders(pr, []byte(body))
	assert.Nil(t, err)
	assert.Equal(t, ebs, bs)
	assert.Equal(t, eas, as)
}

func TestParseGetOrdersInvertedAsksOnly(t *testing.T) {
	body := `
		{
		  "success": true,
		  "data": {
			 "buyOrders": [],
			 "leadCurrencyExponent": 0,
			 "market": "ETH/BTC",
			 "oppositeCurrencyExponent": 0,
			 "orderType": "1",
			 "sellOrders": [
				{
				  "price": "109090909090909091/20000000"
				},
				{
				  "price": "2200000000000000000/300000001"
				},
				{
				  "price": "3200000000000000000/40000002"
				}
			 ],
			 "timeRangeEnd": "2020-01-09T22:36:03+01:00",
			 "timeRangeStart": "2020-01-09T22:36:03+01:00"
		  },
		  "version": "1"
		}
	`
	pr := Pair{Base: Btc, Quote: Eth}

	ts, err := time.Parse(time.RFC3339, "2020-01-09T22:36:03+01:00")

	// expected sell orders/asks
	var eas []Order

	// sells become buys/bids because we flipped the currency pair
	ebs := []Order{
		{
			Pair:      pr,
			OrderType: Bid,
			BaseQ:     &BigInt{big.NewInt(20000000)},
			QuoteQ:    &BigInt{big.NewInt(1)},
			BigPrice:  &BigRat{big.NewRat(20000000, 1)},
			Time:      ts,
		},
		{
			Pair:      pr,
			OrderType: Bid,
			BaseQ:     &BigInt{big.NewInt(300000001)},
			QuoteQ:    &BigInt{big.NewInt(2)},
			BigPrice:  &BigRat{big.NewRat(300000001, 2)},
			Time:      ts,
		},
		{
			Pair:      pr,
			OrderType: Bid,
			BaseQ:     &BigInt{big.NewInt(40000002)},
			QuoteQ:    &BigInt{big.NewInt(3)},
			BigPrice:  &BigRat{big.NewRat(40000002, 3)},
			Time:      ts,
		},
	}
	_, ok := ebs[0].QuoteQ.SetString("109090909090909091", 10)
	assert.True(t, ok)
	_, ok = ebs[0].BigPrice.SetString("20000000/109090909090909091")
	assert.True(t, ok)
	_, ok = ebs[1].QuoteQ.SetString("2200000000000000000", 10)
	assert.True(t, ok)
	_, ok = ebs[1].BigPrice.SetString("300000001/2200000000000000000")
	assert.True(t, ok)
	_, ok = ebs[2].QuoteQ.SetString("3200000000000000000", 10)
	assert.True(t, ok)
	_, ok = ebs[2].BigPrice.SetString("40000002/3200000000000000000")
	assert.True(t, ok)

	bs, as, err := parseGetOrders(pr, []byte(body))
	assert.Nil(t, err)
	assert.Equal(t, ebs, bs)
	assert.Equal(t, eas, as)
}

func TestParseGetOrdersInverted(t *testing.T) {
	body := `
		{
		  "success": true,
		  "data": {
			 "buyOrders": [
				{
				  "price": "109090909090909091/20000000"
				}
			 ],
			 "leadCurrencyExponent": 0,
			 "market": "ETH/BTC",
			 "oppositeCurrencyExponent": 0,
			 "orderType": "1",
			 "sellOrders": [
				{
				  "price": "2200000000000000000/300000001"
				},
				{
				  "price": "3200000000000000000/40000002"
				}
			 ],
			 "timeRangeEnd": "2020-01-09T22:36:03+01:00",
			 "timeRangeStart": "2020-01-09T22:36:03+01:00"
		  },
		  "version": "1"
		}
	`
	pr := Pair{Base: Btc, Quote: Eth}

	ts, err := time.Parse(time.RFC3339, "2020-01-09T22:36:03+01:00")

	// bids become asks because we flipped the currency pair
	eas := []Order{
		{
			Pair:      pr,
			OrderType: Ask,
			BaseQ:     &BigInt{big.NewInt(20000000)},
			QuoteQ:    &BigInt{big.NewInt(1)},
			BigPrice:  &BigRat{big.NewRat(20000000, 1)},
			Time:      ts,
		},
	}
	_, ok := eas[0].QuoteQ.SetString("109090909090909091", 10)
	assert.True(t, ok)
	_, ok = eas[0].BigPrice.SetString("20000000/109090909090909091")
	assert.True(t, ok)

	// asks become bids since we flipped the currencies
	ebs := []Order{
		{
			Pair:      pr,
			OrderType: Bid,
			BaseQ:     &BigInt{big.NewInt(300000001)},
			QuoteQ:    &BigInt{big.NewInt(2)},
			BigPrice:  &BigRat{big.NewRat(300000001, 2)},
			Time:      ts,
		},
		{
			Pair:      pr,
			OrderType: Bid,
			BaseQ:     &BigInt{big.NewInt(40000002)},
			QuoteQ:    &BigInt{big.NewInt(3)},
			BigPrice:  &BigRat{big.NewRat(40000002, 3)},
			Time:      ts,
		},
	}
	_, ok = ebs[0].QuoteQ.SetString("2200000000000000000", 10)
	assert.True(t, ok)
	_, ok = ebs[0].BigPrice.SetString("300000001/2200000000000000000")
	assert.True(t, ok)
	_, ok = ebs[1].QuoteQ.SetString("3200000000000000000", 10)
	assert.True(t, ok)
	_, ok = ebs[1].BigPrice.SetString("40000002/3200000000000000000")
	assert.True(t, ok)

	bs, as, err := parseGetOrders(pr, []byte(body))
	assert.Nil(t, err)
	assert.Equal(t, ebs, bs)
	assert.Equal(t, eas, as)
}

func TestParseGetOrders(t *testing.T) {
	body := `
		{
		  "success": true,
		  "data": {
			 "buyOrders": [
				{
				  "price": "109090909090909091/20000000"
				}
			 ],
			 "leadCurrencyExponent": 0,
			 "market": "ETH/BTC",
			 "oppositeCurrencyExponent": 0,
			 "orderType": "1",
			 "sellOrders": [
				{
				  "price": "2200000000000000000/300000001"
				},
				{
				  "price": "3200000000000000000/40000002"
				}
			 ],
			 "timeRangeEnd": "2020-01-09T22:36:03+01:00",
			 "timeRangeStart": "2020-01-09T22:36:03+01:00"
		  },
		  "version": "1"
		}
	`
	pr := Pair{Base: Eth, Quote: Btc}

	ts, err := time.Parse(time.RFC3339, "2020-01-09T22:36:03+01:00")
	assert.Nil(t, err)

	// expected bids
	ebs := []Order{
		{
			Pair:      pr,
			OrderType: Bid,
			BaseQ:     &BigInt{big.NewInt(1)},
			QuoteQ:    &BigInt{big.NewInt(20000000)},
			BigPrice:  &BigRat{big.NewRat(1, 20000000)},
			Time:      ts,
		},
	}
	_, ok := ebs[0].BaseQ.SetString("109090909090909091", 10)
	assert.True(t, ok)
	_, ok = ebs[0].BigPrice.SetString("109090909090909091/20000000")
	assert.True(t, ok)

	// expected asks
	eas := []Order{
		{
			Pair:      pr,
			OrderType: Ask,
			BaseQ:     &BigInt{big.NewInt(2)},
			QuoteQ:    &BigInt{big.NewInt(300000001)},
			BigPrice:  &BigRat{big.NewRat(2, 300000001)},
			Time:      ts,
		},
		{
			Pair:      pr,
			OrderType: Ask,
			BaseQ:     &BigInt{big.NewInt(3)},
			QuoteQ:    &BigInt{big.NewInt(40000002)},
			BigPrice:  &BigRat{big.NewRat(3, 40000002)},
			Time:      ts,
		},
	}
	_, ok = eas[0].BaseQ.SetString("2200000000000000000", 10)
	assert.True(t, ok)
	_, ok = eas[0].BigPrice.SetString("2200000000000000000/300000001")
	assert.True(t, ok)
	_, ok = eas[1].BaseQ.SetString("3200000000000000000", 10)
	assert.True(t, ok)
	_, ok = eas[1].BigPrice.SetString("3200000000000000000/40000002")
	assert.True(t, ok)

	bs, as, err := parseGetOrders(pr, []byte(body))
	assert.Nil(t, err)
	assert.Equal(t, ebs, bs)
	assert.Equal(t, eas, as)
}

func TestParseGetBalance(t *testing.T) {
	body := `
		{
		  "success": true,
		  "data": {
			 "address": "0x8a7a700f834304e9b390fc2be3fd038abc1eab6e",
			 "balance": 560133890
		  },
		  "version": "1"
		}
	`
	cy := USDT
	bi := big.NewInt(0)
	_, ok := bi.SetString("560133890", 10)
	assert.True(t, ok)

	expected := &Balance{
		Currency: cy,
		Address:  "0x8a7a700f834304e9b390fc2be3fd038abc1eab6e",
		Balance:  &BigInt{bi},
	}
	actual, err := parseGetBalance(cy, []byte(body))
	assert.Nil(t, err)
	expected.Time = actual.Time
	assert.Equal(t, expected, actual)
}

func TestParseGetAvailableBalance(t *testing.T) {
	body := `
		{
		  "success": true,
		  "data": 560133890,
		  "version": "1"
		}
	`
	bi := big.NewInt(0)
	_, ok := bi.SetString("560133890", 10)
	assert.True(t, ok)

	expected := &Balance{
		Currency:  Btc,
		Available: true,
		Balance:   &BigInt{bi},
	}
	actual, err := parseGetAvailableBalance(Btc, []byte(body))
	assert.Nil(t, err)
	expected.Time = actual.Time
	assert.Equal(t, expected, actual)
}

func TestParseGetLastPrice(t *testing.T) {
	body := `
		{
		  "success": true,
		  "data": {
			 "Pair": "ETH/BTC",
			 "prices": [
				{
				  "price": "1400000000000000000/280000000",
				  "time": "2020-01-10T00:59:29.366Z"
				},
				{
				  "price": "1300000000000000000/260000000",
				  "time": "2020-01-09T13:19:22.231Z"
				},
				{
				  "price": "100000000000000000/20000000",
				  "time": "2020-01-06T11:04:36.746Z"
				},
				{
				  "price": "1200000000000000000/200000000",
				  "time": "2020-01-06T05:45:33.969Z"
				},
				{
				  "price": "1000000000000000000/1000000",
				  "time": "2019-12-11T13:09:14.549Z"
				},
				{
				  "price": "10000000000000000/1000000",
				  "time": "2019-12-06T13:16:12.123Z"
				},
				{
				  "price": "10000000000000000/1000000",
				  "time": "2019-12-06T12:18:55.104Z"
				},
				{
				  "price": "10000000000000000/1000000",
				  "time": "2019-12-06T11:00:39.415Z"
				},
				{
				  "price": "10000000000000000/1000000",
				  "time": "2019-12-06T10:18:37.291Z"
				},
				{
				  "price": "10000000000000000/1000000",
				  "time": "2019-12-06T09:55:55.47Z"
				},
				{
				  "price": "1000000000000000000/1000000",
				  "time": "2019-12-06T09:42:13.511Z"
				},
				{
				  "price": "100000000000000000/500000",
				  "time": "2019-12-06T07:37:52.033Z"
				},
				{
				  "price": "10000000000000000/100000",
				  "time": "2019-12-02T16:15:28.364Z"
				},
				{
				  "price": "1000000000000000000/5000000",
				  "time": "2019-12-02T13:47:15.425Z"
				},
				{
				  "price": "1000000000000000000/2000000",
				  "time": "2019-12-02T10:29:22.048Z"
				}
			 ]
		  },
		  "version": "1"
		}
	`
	br := big.NewRat(1, 1)
	_, ok := br.SetString("1400000000000000000/280000000")
	assert.True(t, ok)
	pr := Pair{Base: Eth, Quote: Btc}
	ts, err := time.Parse(time.RFC3339, "2020-01-10T00:59:29.366Z")
	assert.Nil(t, err)
	expected := &Price{
		Pair:  pr,
		Price: &BigRat{br},
		Time:  ts,
	}

	actual, err := parseGetLastPrice(pr, []byte(body))
	assert.Nil(t, err)
	assert.Equal(t, expected, actual)
}

func TestParseGetLastPriceInverted(t *testing.T) {
	body := `
		{
		  "success": true,
		  "data": {
			 "Pair": "ETH/BTC",
			 "prices": [
				{
				  "price": "1400000000000000000/280000000",
				  "time": "2020-01-10T00:59:29.366Z"
				},
				{
				  "price": "1300000000000000000/260000000",
				  "time": "2020-01-09T13:19:22.231Z"
				},
				{
				  "price": "100000000000000000/20000000",
				  "time": "2020-01-06T11:04:36.746Z"
				},
				{
				  "price": "1200000000000000000/200000000",
				  "time": "2020-01-06T05:45:33.969Z"
				},
				{
				  "price": "1000000000000000000/1000000",
				  "time": "2019-12-11T13:09:14.549Z"
				},
				{
				  "price": "10000000000000000/1000000",
				  "time": "2019-12-06T13:16:12.123Z"
				},
				{
				  "price": "10000000000000000/1000000",
				  "time": "2019-12-06T12:18:55.104Z"
				},
				{
				  "price": "10000000000000000/1000000",
				  "time": "2019-12-06T11:00:39.415Z"
				},
				{
				  "price": "10000000000000000/1000000",
				  "time": "2019-12-06T10:18:37.291Z"
				},
				{
				  "price": "10000000000000000/1000000",
				  "time": "2019-12-06T09:55:55.47Z"
				},
				{
				  "price": "1000000000000000000/1000000",
				  "time": "2019-12-06T09:42:13.511Z"
				},
				{
				  "price": "100000000000000000/500000",
				  "time": "2019-12-06T07:37:52.033Z"
				},
				{
				  "price": "10000000000000000/100000",
				  "time": "2019-12-02T16:15:28.364Z"
				},
				{
				  "price": "1000000000000000000/5000000",
				  "time": "2019-12-02T13:47:15.425Z"
				},
				{
				  "price": "1000000000000000000/2000000",
				  "time": "2019-12-02T10:29:22.048Z"
				}
			 ]
		  },
		  "version": "1"
		}
	`
	// invert the price pair
	pr := Pair{Base: Btc, Quote: Eth}
	br := big.NewRat(1, 1)
	_, ok := br.SetString("280000000/1400000000000000000")
	assert.True(t, ok)
	ts, err := time.Parse(time.RFC3339, "2020-01-10T00:59:29.366Z")
	assert.Nil(t, err)

	expected := &Price{
		Pair:  pr,
		Price: &BigRat{br},
		Time:  ts,
	}

	actual, err := parseGetLastPrice(pr, []byte(body))
	assert.Nil(t, err)
	assert.Equal(t, expected, actual)
}

func TestParseMyOrders(t *testing.T) {
	body := `
		{
		  "success": true,
		  "data": {
			 "buyOrders": [
				{
				  "created": "2020-01-15T07:04:23.968863899Z",
				  "dark": false,
				  "filled": null,
				  "orderId": "orderId_822944d7-d0f0-4c03-9af9-6b310f3ebe2d",
				  "orderMarket": "ETH/BTC",
				  "price": "4999500000000000000/999900000000",
				  "quantity": null,
				  "validUntil": "2020-01-15T07:04:33.913017819Z"
				}
			 ],
			 "leadCurrencyExponent": 18,
			 "market": "ETH/BTC",
			 "oppositeCurrencyExponent": 18,
			 "orderType": "",
			 "sellOrders": [
				{
				  "created": "2020-01-15T07:04:24.023810552Z",
				  "dark": false,
				  "filled": null,
				  "orderId": "orderId_b5cf56d0-e711-4635-896d-e50adae32467",
				  "orderMarket": "ETH/BTC",
				  "price": "6000000000000000066/9000000000099",
				  "quantity": null,
				  "validUntil": "2020-01-15T07:04:33.986925146Z"
				}
			 ],
			 "timeRangeEnd": "2020-01-15T22:36:03+01:00",
			 "timeRangeStart": "2020-01-09T22:36:03+01:00"
		  },
		  "version": "1"
		}
	`
	pr := Pair{Base: Eth, Quote: Btc}

	// expected bids
	cd, err := time.Parse(time.RFC3339, "2020-01-15T07:04:23.968863899Z")
	assert.Nil(t, err)
	vu, err := time.Parse(time.RFC3339, "2020-01-15T07:04:33.913017819Z")
	assert.Nil(t, err)

	ebs := []Order{
		{
			Pair:        pr,
			OrderType:   Bid,
			BaseQ:       &BigInt{big.NewInt(1)},
			QuoteQ:      &BigInt{big.NewInt(2)},
			BigPrice:    &BigRat{big.NewRat(1, 2)},
			Time:        cd,
			ExpiresAt:   vu,
			EID:         "orderId_822944d7-d0f0-4c03-9af9-6b310f3ebe2d",
			OrderStatus: Placed,
		},
	}
	_, ok := ebs[0].BaseQ.SetString("4999500000000000000", 10)
	assert.True(t, ok)
	_, ok = ebs[0].QuoteQ.SetString("999900000000", 10)
	assert.True(t, ok)
	_, ok = ebs[0].BigPrice.SetString("4999500000000000000/999900000000")
	assert.True(t, ok)

	// expected asks
	cd, err = time.Parse(time.RFC3339, "2020-01-15T07:04:24.023810552Z")
	assert.Nil(t, err)
	vu, err = time.Parse(time.RFC3339, "2020-01-15T07:04:33.986925146Z")
	assert.Nil(t, err)
	eas := []Order{
		{
			Pair:        pr,
			OrderType:   Ask,
			BaseQ:       &BigInt{big.NewInt(1)},
			QuoteQ:      &BigInt{big.NewInt(2)},
			BigPrice:    &BigRat{big.NewRat(1, 2)},
			Time:        cd,
			ExpiresAt:   vu,
			EID:         "orderId_b5cf56d0-e711-4635-896d-e50adae32467",
			OrderStatus: Placed,
		},
	}
	_, ok = eas[0].BaseQ.SetString("6000000000000000066", 10)
	assert.True(t, ok)
	_, ok = eas[0].QuoteQ.SetString("9000000000099", 10)
	assert.True(t, ok)
	_, ok = eas[0].BigPrice.SetString("6000000000000000066/9000000000099")
	assert.True(t, ok)

	bs, as, err := parseMyOrders(pr, []byte(body))
	assert.Nil(t, err)
	assert.Equal(t, ebs, bs)
	assert.Equal(t, eas, as)
}

func TestParseMyOrdersInverted(t *testing.T) {
	body := `
		{
		  "success": true,
		  "data": {
			 "buyOrders": [
				{
				  "created": "2020-01-15T07:04:23.968863899Z",
				  "dark": false,
				  "filled": null,
				  "orderId": "orderId_822944d7-d0f0-4c03-9af9-6b310f3ebe2d",
				  "orderMarket": "ETH/BTC",
				  "price": "4999500000000000000/999900000000",
				  "quantity": null,
				  "validUntil": "2020-01-15T07:04:33.913017819Z"
				}
			 ],
			 "leadCurrencyExponent": 18,
			 "market": "ETH/BTC",
			 "oppositeCurrencyExponent": 18,
			 "orderType": "",
			 "sellOrders": [
				{
				  "created": "2020-01-15T07:04:24.023810552Z",
				  "dark": false,
				  "filled": null,
				  "orderId": "orderId_b5cf56d0-e711-4635-896d-e50adae32467",
				  "orderMarket": "ETH/BTC",
				  "price": "6000000000000000066/9000000000099",
				  "quantity": null,
				  "validUntil": "2020-01-15T07:04:33.986925146Z"
				}
			 ],
			 "timeRangeEnd": "2020-01-15T22:36:03+01:00",
			 "timeRangeStart": "2020-01-09T22:36:03+01:00"
		  },
		  "version": "1"
		}
	`
	pr := Pair{Base: Btc, Quote: Eth}

	cd, err := time.Parse(time.RFC3339, "2020-01-15T07:04:23.968863899Z")
	assert.Nil(t, err)
	vu, err := time.Parse(time.RFC3339, "2020-01-15T07:04:33.913017819Z")
	assert.Nil(t, err)
	// bids become asks because we flipped the currencies
	eas := []Order{
		{
			Pair:        pr,
			OrderType:   Ask,
			BaseQ:       &BigInt{big.NewInt(1)},
			QuoteQ:      &BigInt{big.NewInt(2)},
			BigPrice:    &BigRat{big.NewRat(1, 2)},
			Time:        cd,
			ExpiresAt:   vu,
			EID:         "orderId_822944d7-d0f0-4c03-9af9-6b310f3ebe2d",
			OrderStatus: Placed,
		},
	}
	_, ok := eas[0].BaseQ.SetString("999900000000", 10)
	assert.True(t, ok)
	_, ok = eas[0].QuoteQ.SetString("4999500000000000000", 10)
	assert.True(t, ok)
	_, ok = eas[0].BigPrice.SetString("999900000000/4999500000000000000")
	assert.True(t, ok)

	cd, err = time.Parse(time.RFC3339, "2020-01-15T07:04:24.023810552Z")
	assert.Nil(t, err)
	vu, err = time.Parse(time.RFC3339, "2020-01-15T07:04:33.986925146Z")
	assert.Nil(t, err)
	// asks become bids because we flipped the currencies
	ebs := []Order{
		{
			Pair:        pr,
			OrderType:   Bid,
			BaseQ:       &BigInt{big.NewInt(1)},
			QuoteQ:      &BigInt{big.NewInt(2)},
			BigPrice:    &BigRat{big.NewRat(1, 2)},
			Time:        cd,
			ExpiresAt:   vu,
			EID:         "orderId_b5cf56d0-e711-4635-896d-e50adae32467",
			OrderStatus: Placed,
		},
	}
	_, ok = ebs[0].BaseQ.SetString("9000000000099", 10)
	assert.True(t, ok)
	_, ok = ebs[0].QuoteQ.SetString("6000000000000000066", 10)
	assert.True(t, ok)
	_, ok = ebs[0].BigPrice.SetString("9000000000099/6000000000000000066")
	assert.True(t, ok)

	bs, as, err := parseMyOrders(pr, []byte(body))
	assert.Nil(t, err)
	assert.Equal(t, ebs, bs)
	assert.Equal(t, eas, as)
}

func TestIsValidCurrency(t *testing.T) {
	var c Currency
	assert.False(t, c.IsValid())
}

func TestIsValidCurrencyEth(t *testing.T) {
	var c Currency = Eth
	assert.True(t, c.IsValid())
}

func TestIsValidPair(t *testing.T) {
	var p Pair
	assert.False(t, p.IsValid())
}

func TestIsValidPairBtcEth(t *testing.T) {
	var p Pair = Pair{Btc, Eth}
	assert.True(t, p.IsValid())
}

func TestParseSwaps(t *testing.T) {
	data := `
		{
		  "success": true,
		  "data": [
			 {
				"progress": 100,
				"pair": [
				  "BTC",
				  "USDT"
				],
				"time": 1579788821,
				"lastEvent": 3,
				"status": "Swapped",
				"statusColor": "green",
				"price": "91649999/7847775551",
				"participant": false,
				"action": "INIT/SUCCESS",
				"error": "",
				"orderAction": 0,
				"id": "orderId_c6160f89-87f1-4eae-9e23-f083ad373e7a",
				"progressState": {
				  "InitiatorSend": {
					 "detected": "2020-01-23T14:13:41.864715043Z",
					 "confirmations": 1,
					 "lastConfirmation": "2020-01-23T14:13:45.910349905Z",
					 "txHash": "6666349990d8f901f0712fd1ff036025a826fe50a0facf5d8abb8138fd152be2",
					 "submitted": "2020-01-23T14:13:41.864715218Z",
					 "skipped": false,
					 "timeoutSeconds": 0
				  },
				  "AllowanceReset": {
					 "detected": "0001-01-01T00:00:00Z",
					 "confirmations": 0,
					 "lastConfirmation": "0001-01-01T00:00:00Z",
					 "txHash": "",
					 "submitted": "0001-01-01T00:00:00Z",
					 "skipped": false,
					 "timeoutSeconds": 0
				  },
				  "SetAllowanceAmount": {
					 "detected": "0001-01-01T00:00:00Z",
					 "confirmations": 0,
					 "lastConfirmation": "0001-01-01T00:00:00Z",
					 "txHash": "",
					 "submitted": "0001-01-01T00:00:00Z",
					 "skipped": false,
					 "timeoutSeconds": 0
				  },
				  "ParticipantSend": {
					 "detected": "2020-01-23T14:13:45.913492539Z",
					 "confirmations": 2,
					 "lastConfirmation": "2020-01-23T14:13:53.933468366Z",
					 "txHash": "0000000000000000000000009b5b7043d4e7fc964da42faf666d439487ed4e89",
					 "submitted": "2020-01-23T14:13:45.910713619Z",
					 "skipped": false,
					 "timeoutSeconds": 21600
				  },
				  "InitiatorRedeem": {
					 "detected": "2020-01-23T14:13:59.962777444Z",
					 "confirmations": 2,
					 "lastConfirmation": "2020-01-23T14:14:03.975662322Z",
					 "txHash": "0x4f52a3bac9c7330645fada5b97885a7afe3e0a7eef7c3646c47816e462aae5c8",
					 "submitted": "2020-01-23T14:13:53.951212898Z",
					 "skipped": false,
					 "timeoutSeconds": 21600
				  },
				  "ParticipantRedeem": {
					 "detected": "0001-01-01T00:00:00Z",
					 "confirmations": 0,
					 "lastConfirmation": "0001-01-01T00:00:00Z",
					 "txHash": "",
					 "submitted": "0001-01-01T00:00:00Z",
					 "skipped": false,
					 "timeoutSeconds": 0
				  },
				  "Recover": {
					 "detected": "0001-01-01T00:00:00Z",
					 "confirmations": 0,
					 "lastConfirmation": "0001-01-01T00:00:00Z",
					 "txHash": "",
					 "submitted": "0001-01-01T00:00:00Z",
					 "skipped": false,
					 "timeoutSeconds": 0
				  },
				  "Redeem": {
					 "detected": "0001-01-01T00:00:00Z",
					 "confirmations": 0,
					 "lastConfirmation": "0001-01-01T00:00:00Z",
					 "txHash": "",
					 "submitted": "0001-01-01T00:00:00Z",
					 "skipped": false,
					 "timeoutSeconds": 0
				  }
				},
				"leadCurrencyConfirmations": 1,
				"oppositeCurrencyConfirmations": 2,
				"nextRetry": "0001-01-01T00:00:00Z"
			 }
		  ],
		  "version": "1"
		}
	`
	expected := []Swap{
		{
			Progress:       100,
			Pair:           "BTC/USDT",
			Time:           time.Unix(1579788821, 0).UTC(),
			Status:         "Swapped",
			Quantities:     "91649999/7847775551",
			Action:         "INIT/SUCCESS",
			OrderID:        "orderId_c6160f89-87f1-4eae-9e23-f083ad373e7a",
			BConfirmations: 1,
			QConfirmations: 2,
		},
	}
	s, err := parseGetSwaps([]byte(data))
	assert.Nil(t, err)
	require.NotNil(t, s)
	assert.Equal(t, expected, s)
}
