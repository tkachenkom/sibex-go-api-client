# Go library for the SIBEX API

Hello! This is the [go](https://golang.org/) library for the [SIBEX REST API](https://docs.test.sibex.io/api).

It is still very much work in progress and growing fast.
We welcome both issues and merge requests.

Have fun with it :)
